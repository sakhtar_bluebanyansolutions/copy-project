/**
 * @NApiVersion 2.x
 * @NScriptType UserEventScript
 * @NModuleScope public
 * @author Keith B
 * @version 0.2.0
 */

define(["N/runtime", "N/search", "N/url", "N/search", "N/record"], function (runtime, searchModule, url, search, record) {

    /**
     * Function called before load of the project record
     * 
     * @governance 0 Units
     * @param {Object} context - context of the request
     */
    function beforeLoad(context) {

        context.form.clientScriptModulePath = './BB.CS.ProjectAdditional';

        var record = context.newRecord;

        addCopyProjectButton(context, record);
      addGetSiteDevicesButton(context.form, record)

    }

    /**
     * Function shows the copy project button to admins
     * 
     * @governance 0 Units
     * @param {Object} context - context of the request
     * @param {Object} newRecord - new project record
     */
    function addCopyProjectButton(context, newRecord) {
        var isTemplate = newRecord.getValue('custentity_bb_is_project_template')
        if (context.type == context.UserEventType.VIEW) {


            var copyProjectBtnSetting = search.lookupFields({
                type: 'customrecord_bb_solar_success_configurtn',
                id: 1,
                columns: ['custrecord_bb_shw_cp_prj_btn']
            });

            log.debug('copyProjectBtnSetting', copyProjectBtnSetting);
            if (copyProjectBtnSetting.custrecord_bb_shw_cp_prj_btn && !isTemplate) {
                var copyTo = newRecord.getValue({ fieldId: "custentity_bb_copy_to" });
                var roleId = runtime.getCurrentUser().role;
                var suiteletUrl = url.resolveScript({
                    scriptId: 'customscript_bb_sl_copyproject',
                    deploymentId: 'customdeploy_bb_sl_copyproject',
                    params: {
                        recordId: context.newRecord.id
                    }
                });

                var fullURL = "https://" + url.resolveDomain({ hostType: url.HostType.APPLICATION }) + suiteletUrl;

                context.form.addButton({
                    id: 'custpage_copyproject',
                    label: 'Copy Project',
                    functionName: "callCopyProjectSuitelet"
                    // functionName: "windown.open(\'"+ fullURL+ "\')"
                });

                if (copyTo && roleId != 3) {
                    context.form.removeButton({ id: "edit" })
                }
            }

        }
    }

    function addGetSiteDevicesButton(form, rec) {
        var proj = record.load({
            type: 'job',
            id: rec.id,
            isDynamic: true,
        })
        var sublist = form.getSublist({
            id: 'recmachcustrecord_bb_ss_device_proj'
        });// subtab internal ID on where you would like the button to show
        sublist.addButton({
            id: 'custpage_buttonid',
            label: 'Load Devices',
            functionName: "callLoadDevices"
        });
    }

    return {
        beforeLoad: beforeLoad
    };
});